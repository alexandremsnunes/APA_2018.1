#--- ALUNOS -   WALSAN JADSON, ALEXANDRE NUNES ---

#------- IMPORTANDO BIBLIOTECAS -------
import sys
import random

flagVND = 0
alpha = 1

#------- DEFININDO FUNCOES -------

def imprimirMatriz(matrizDeAdjacencia):
    for linha in matrizDeAdjacencia:
        print (linha)

def buscarMenor(matrizDeAdjacencia,solucao,origem,numeroDeVertices):
    i = float("inf")
    for destino in range(0, numeroDeVertices):
        if( (matrizDeAdjacencia[origem][destino] != 0) and (matrizDeAdjacencia[origem][destino] < i) and (destino not in solucao) ):
            i = matrizDeAdjacencia[origem][destino]
    return i

def buscarMaior(matrizDeAdjacencia,solucao,origem,numeroDeVertices):
    i = 0
    for destino in range(0, numeroDeVertices):
        if( (matrizDeAdjacencia[origem][destino] != 0) and (matrizDeAdjacencia[origem][destino] > i) and (destino not in solucao) ):
            i = matrizDeAdjacencia[origem][destino]
    return i

def copiar(vetor):
    saida = []
    for i in range(0,len(vetor)):
        saida.append(vetor[i])

    return saida

def calcularCusto(matrizDeAdjacencia, listaSolucao):
    custo = 0
    for i in range(0, len(listaSolucao)-1):
        custo += matrizDeAdjacencia[listaSolucao[i]][listaSolucao[i+1]]
    return custo

def criarMatrizQuadrada(n, matriz):
    for i in range(0, n):
        matriz.append([])
        for j in range(0, n):
            matriz[i].append(0)

def reinsertion(matrizDeAdjacencia, listaSolucao, custo):
    for j in range(1, len(listaSolucao)):
        auxListaSolucao = copiar(listaSolucao)
        for i in range(j, len(listaSolucao)-1):
            aux = auxListaSolucao[i]
            auxListaSolucao[i] = auxListaSolucao[i+1]
            auxListaSolucao[i+1] = aux
            novoCusto = calcularCusto(matrizDeAdjacencia, auxListaSolucao)
            if(novoCusto < custo):
                custo = novoCusto
                bestlistaSolucao = copiar(auxListaSolucao)
    return bestlistaSolucao

def twoOPT(matrizDeAdjacencia, listaSolucao, custo):
    for i in range(1, (len(listaSolucao)-1)):
        for j in range((i+1),len(listaSolucao)):
            auxlistaSolucao = copiar(listaSolucao)
            aux = auxlistaSolucao[i]
            auxlistaSolucao[i] = auxlistaSolucao[j]
            auxlistaSolucao[j] = aux
            novoCusto = calcularCusto(matrizDeAdjacencia, auxlistaSolucao)
            if(novoCusto < custo):
                custo = novoCusto
                bestlistaSolucao = copiar(auxlistaSolucao)
    return bestlistaSolucao

def construirHeuristica(numeroDeVertices, matrizDeAdjacencia, listaSolucao):
    origem = 0
    listaSolucao.append(origem)
    menorDistancia = 100000000
    for i in range(0,numeroDeVertices):
        auxDestino = 0
        for destino in range(0, numeroDeVertices):
            if( (matrizDeAdjacencia[origem][destino] != 0) and (matrizDeAdjacencia[origem][destino] < menorDistancia) and (destino not in listaSolucao) ):
                menorDistancia = matrizDeAdjacencia[origem][destino]
                auxDestino = destino
        listaSolucao.append(auxDestino)
        origem = auxDestino
        menorDistancia = 100000000
    listaSolucao.pop()

def vnd(custo, matrizDeAdjacencia, listaSolucao, flagVND):
    parada = 0
    listaSolucao1 = copiar(listaSolucao)
    while(parada < 10):
        if ( flagVND == 0):
           
            try:
                listaSolucao1 = reinsertion(matrizDeAdjacencia,listaSolucao1,custo)
                custo = calcularCusto(matrizDeAdjacencia,listaSolucao1)
                print("custo depois da reinsertion: ", custo)
                
            except UnboundLocalError as identifier:
                flagVND = 1
                parada += 1
        if( flagVND == 1 ):
            try:
                listaSolucao1 = twoOPT(matrizDeAdjacencia,listaSolucao1,custo)
                custo = calcularCusto(matrizDeAdjacencia,listaSolucao1)
                print("custo depois da twoOPT: ", custo)
            except UnboundLocalError as identifier:
                flagVND = 0
                parada += 1
    return listaSolucao1

def construcaoGRASP(matrizDeAdjacencia, alpha, listaSolucao, numeroDeVertices):
    origem = 0
    listaSolucaoAux = []
    listaSolucaoAux.append(origem)
    
    for i in range(0,numeroDeVertices-1):
        #print("_____________________________________________")
        menorDistancia = buscarMenor(matrizDeAdjacencia,listaSolucaoAux,origem,numeroDeVertices)
        maiorDistancia = buscarMaior(matrizDeAdjacencia,listaSolucaoAux,origem,numeroDeVertices)
        #print("Menor distancia:", menorDistancia)
        #print("Maior distancia:", maiorDistancia)
        
        lcr = menorDistancia + ((maiorDistancia - menorDistancia) * alpha)
        #print("valor lcr:", lcr)
        listaIndicesLCR = []

        '''
        for destino in range(0, numeroDeVertices):
            if(matrizDeAdjacencia[origem][destino] <= lcr):
                
        '''
        for destino in range(0, numeroDeVertices):
            if( (matrizDeAdjacencia[origem][destino] <= lcr) and matrizDeAdjacencia[origem][destino] != 0 and (destino not in listaSolucaoAux)):
                listaIndicesLCR.append(destino)
                #print("destino:",destino)
        

        indiceAleatorio = random.randint(0, len(listaIndicesLCR)-1)
        indiceAleatorio = 0
        #print("listaIndicesLCR:", listaIndicesLCR)
        origem = listaIndicesLCR[indiceAleatorio]
        listaSolucaoAux.append(origem)
        #print("origem: ",origem)
        #print("listaSolucaoAux:",listaSolucaoAux)
        #print("_____________________________________________")

    return listaSolucaoAux

#------- DEFININDO CLASSES -------


#------- ENTRADA -------

'''
exemplo de chamada a ser executada pelo terminal:
>python CaxeiroViajante.py enetradaCV.txt

'''
arquivoDeEntrada = sys.argv[1]
arquivo = open(arquivoDeEntrada, 'r')
numeroDeVertices = int(arquivo.readline())

matrizDeAdjacencia = []
criarMatrizQuadrada(numeroDeVertices, matrizDeAdjacencia)

#preenchendo a matriz de adjacencia
for i in range(1, numeroDeVertices):
    linha = arquivo.readline()
    elementos = linha.rsplit()
    aux = 0
    for j in range(i, numeroDeVertices):
        matrizDeAdjacencia[i-1][j] = int(elementos[aux])
        matrizDeAdjacencia[j][i-1] = int(elementos[aux])
        aux += 1


#------- PROCESSAMENTO -------
listaSolucao = []
listaSolucaoHeu = []
# CONTRUCAO DA EURISTICA
'''
listaSolucao = []
origem = 0
listaSolucao.append(origem)
menorDistancia = 100000000
for i in range(0,numeroDeVertices):
    auxDestino = 0
    for destino in range(0, numeroDeVertices):
        if( (matrizDeAdjacencia[origem][destino] != 0) and (matrizDeAdjacencia[origem][destino] < menorDistancia) and (destino not in listaSolucao) ):
            menorDistancia = matrizDeAdjacencia[origem][destino]
            auxDestino = destino
    listaSolucao.append(auxDestino)
    origem = auxDestino
    menorDistancia = 100000000
listaSolucao.pop()
'''

print("INICIO Heuristica")
construirHeuristica(numeroDeVertices,matrizDeAdjacencia,listaSolucaoHeu)
custoFinalHeu = calcularCusto(matrizDeAdjacencia,listaSolucaoHeu)
listaSolucaoHeu = vnd(custoFinalHeu,matrizDeAdjacencia,listaSolucaoHeu,flagVND)


print("FIM Heuristica")
print("############################################")
print("")
# CONSTRUCAO DA META-HEURISTICA
custoFinalGRASP = float("inf")
solucaoFinalGrasp = []

print("INICIO META Heuristica")
for i in range(0, 20):
    #print("##########################################################")
    listaSolucao = construcaoGRASP(matrizDeAdjacencia, alpha, listaSolucao, numeroDeVertices)
    print("Solucao construida no GRASP: ", listaSolucao)
    custo = calcularCusto(matrizDeAdjacencia,listaSolucao)
    listaSolucao =  vnd(custo,matrizDeAdjacencia,listaSolucao,flagVND)
    custo = calcularCusto(matrizDeAdjacencia,listaSolucao)

    if(custo < custoFinalGRASP):
        solucaoFinalGrasp = copiar(listaSolucao)  
        custoFinalGRASP = custo
    #print("##########################################################")
    print("_________________________________________________________________________________")

print("FIM META Heuristica")
print("############################################")
print("")

#print("solcuao:")
#print(listaSolucao)

# CALCULAR O CUSTO DO CAMINHO

#custo = calcularCusto(matrizDeAdjacencia, listaSolucao)
#print("custo:",custo)

# METODO VND
'''
parada = 0
while(parada < 10):
    if ( flagVND == 0):
        try:
            listaSolucao = reinsertion(matrizDeAdjacencia,listaSolucao,custo)
            custo = calcularCusto(matrizDeAdjacencia,listaSolucao)
            print("custo depois da reinsertion: ", custo)
        except UnboundLocalError as identifier:
            flagVND = 1
            parada += 1
    if( flagVND == 1 ):
        try:
            listaSolucao = twoOPT(matrizDeAdjacencia,listaSolucao,custo)
            custo = calcularCusto(matrizDeAdjacencia,listaSolucao)
            print("custo depois da twoOPT: ", custo)
        except UnboundLocalError as identifier:
            flagVND = 0
            parada += 1
'''
#vnd(custo, matrizDeAdjacencia, listaSolucao, flagVND)

#------- SAIDA -------

print("Resultado Final:")
print("Vetor Solucao resultante da Heuristica: ",listaSolucaoHeu)
custoFinalHeu = calcularCusto(matrizDeAdjacencia,listaSolucaoHeu)
print("Custo final resultante da Heuristica:",custoFinalHeu)
print("")
print("Vetor Solucao resultante do GRASP:", solucaoFinalGrasp)
print("Custo final resultante do GRASP:", custoFinalGRASP)
#imprimirMatriz(matrizDeAdjacencia )
