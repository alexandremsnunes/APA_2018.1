#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

void criaVetordeEntrada(char const *argumento[]);

int partition( int a[], int l, int r) {
   int pivot, i, j, t;
   pivot = a[l];
   i = l; j = r+1;
		
   while(1){
   		do ++i; while( a[i] <= pivot && i <= r );
   		do --j; while( a[j] > pivot );
   		if( i >= j ) break;
   		t = a[i]; a[i] = a[j]; a[j] = t;
   }
   t = a[l]; a[l] = a[j]; a[j] = t;
   return j;
}

void quickSort( int vetor[], int l, int r){
   int j;

   if( l < r ){
   	   j = partition( vetor, l, r);
       quickSort( vetor, l, j-1);
       quickSort( vetor, j+1, r);
   }
}

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	tempos[0] = clock();
	quickSort(vetorentrada, 0, (tam-1)); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (QuickSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
