#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

void criaVetordeEntrada(char const *argumento[]);


void merge(int vetor[], int p, int q, int r) {
    
    int L1 = p, 
    	R1 = q+1, 
    	aux = 0, 
    	n = r-p+1;

    int v[n];
   
    while(L1 <= q && R1 <= r){
        if(vetor[L1] < vetor[R1]) {
            v[aux] = vetor[L1];
            L1++;
        }
        else {
            v[aux] = vetor[R1];
            R1++;
        }
        aux++;
    }
   
    while(L1 <= q){  
        v[aux] = vetor[L1];
        aux++;
        L1++;
    }
    while(R1 <= r) {  
        v[aux] = vetor[R1];
        aux++;
        R1++;
    }

    for(aux = p; aux <= r; aux++){    
        vetor[aux] = v[aux-p];
    }
    
    
}


void mergeSort(int vetor[], int p, int r){
	if(p < r){
		int q = ((p + r))/2;
		//printf("entrando no PRIMEIRO mergeSort\n");
		mergeSort(vetor,p,q);
		//printf("entrando no SEGUNDO mergeSort\n");
		mergeSort(vetor,(q+1),r);
		//printf("entrando no merge\n");
		merge(vetor,p,q,r);
	}
}


int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d\n", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	/*printf("\nentrada:");
	for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	tempos[0] = clock();
	mergeSort(vetorentrada,0,(tam-1)); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	
	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (MergeSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
