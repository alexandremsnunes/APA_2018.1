#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>


void criaVetordeEntrada(char const *argumento[]);

void selectionSort(int vetor[], int tam){
	int i, j, menor = 0,indice = 0; 

	for (i = 0; i < tam; i++){
		
		menor = vetor[i];
		indice = i;

		for (j = i+1; j < tam; j++){
			if (vetor[j] < menor){
				indice = j;
				menor = vetor[j];
			}
		}

		vetor[indice] = vetor[i];
		vetor[i] = menor;
		
	}
	
}

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);

	printf("\nFim\n");

	return 0;
}


void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	tempos[0] = clock();
	selectionSort(vetorentrada, tam); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (SelectionSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
