#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

typedef struct $
{
	int verticeA;   // Vertice Origem
    int verticeB;    // Vertice Destino
    int peso; // peso ou custo da aresta
}tAresta;

void criaVetordeEntrada(char const *argumento[]);
void trocar(tAresta v[], int j, int aposJ);
void minHeapify(tAresta vetor[], int pos, int tamanhoDoVetor);
void buildMaxHeap(tAresta v[], int tam);

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void encontraMST(tAresta vetorentrada[],int tam, int vertices){
    buildMaxHeap(vetorentrada,tam);
    int n = tam,
    	MST = 0,
 	 	menor,
 	 	aux;
 	
 	for (int i = tam - 1; i > 0; i--) {
       
       trocar(vetorentrada, i, 0);

       if (vetorentrada[i].verticeA != vetorentrada[i].verticeB && vetorentrada[i].peso > 0 ){
       		MST += vetorentrada[i].peso; // Marca a aresta adicionando o peso na solocao

       		//Ver o vertice menor e o maior respectivamente 
       		menor = vetorentrada[i].verticeA < vetorentrada[i].verticeB ? vetorentrada[i].verticeA : vetorentrada[i].verticeB;
       		aux = vetorentrada[i].verticeA < vetorentrada[i].verticeB ?   vetorentrada[i].verticeB : vetorentrada[i].verticeA;
       		
       		for (int j = 0; j < tam; j++){
       			if (vetorentrada[j].verticeA == aux){
       				vetorentrada[j].verticeA = menor;
       			}
       			if (vetorentrada[j].verticeB == aux){
       				vetorentrada[j].verticeB = menor;
       			}	
       		}
       		//printf("menor %d maior %d\n",menor,aux);
       }
       
    	minHeapify(vetorentrada, 0, --n);

    } 



    printf("\nO valor da MST: %d\n",MST);
   /*for (int i = 0; i < tam; ++i)
     {
     	printf("\nA: %d B: %d peso: %d",vetorentrada[i].verticeA,vetorentrada[i].verticeB,vetorentrada[i].peso);
     }*/
}

void trocar(tAresta v[], int j, int aposJ) {
    tAresta aux = v[j];
    v[j] = v[aposJ];
    v[aposJ] = aux;
}

void minHeapify(tAresta vetor[], int pos, int tamanhoDoVetor) {
 
    int min = 2 * pos + 1, right = min + 1;
    if (min < tamanhoDoVetor) {

 		
        if (right < tamanhoDoVetor && vetor[min].peso > vetor[right].peso) {
            min = right;
        }
 
        if (vetor[min].peso < vetor[pos].peso) {
            trocar(vetor, min, pos);
            minHeapify(vetor, min, tamanhoDoVetor);
        }
    }
}
 
void buildMaxHeap(tAresta v[], int tam) {
    for (int i = tam / 2 - 1; i >= 0; i--) {
        minHeapify(v, i, tam);
    } 
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i,
		j,
		c = 0,
	 	aux,
	 	tam,
	 	vertices;
	
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &vertices);
		
	}

	tam = ((vertices-1)*vertices)/2;
	//printf("\nNumero de vertices: %d - Tamanho do vetor: %d", vertices, tam);
	tAresta vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i].peso = aux;
				vetorentrada[i].verticeA = 0;
				vetorentrada[i].verticeB = 0; 
		}
	}


	printf("\n");
	for(i = 0; i < vertices;i++){
		for(j = i+1; j < vertices; j++){
			vetorentrada[c].verticeA = i;
			vetorentrada[c].verticeB = j;
			//printf("\nA: %d B: %d custo: %d",vetorentrada[c].verticeA,vetorentrada[c].verticeB,vetorentrada[c].peso);
			c++; 
		
			
		}
		//printf("\n");
	}

	tempos[0] = clock();
	encontraMST(vetorentrada, tam, vertices); //Chamada do algoritmo que vai encontrar a arvore espalhada minima MST
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i].peso);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i].peso);	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gasto: %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}