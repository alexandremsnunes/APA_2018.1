#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

void criaVetordeEntrada(char const *argumento[]);

void trocar(int v[], int j, int aposJ) {
    int aux = v[j];
    v[j] = v[aposJ];
    v[aposJ] = aux;
}

void maxHeapify(int vetor[], int pos, int tamanhoDoVetor) {
 
    int max = 2 * pos + 1, right = max + 1;
    if (max < tamanhoDoVetor) {
 
 		
        if (right < tamanhoDoVetor && vetor[max] < vetor[right]) {
            max = right;
        }
 
        if (vetor[max] > vetor[pos]) {
            trocar(vetor, max, pos);
            maxHeapify(vetor, max, tamanhoDoVetor);
        }
    }
}
 
void buildMaxHeap(int v[], int tam) {
    for (int i = tam / 2 - 1; i >= 0; i--) {
        maxHeapify(v, i, tam);
    } 
}

void heapSort(int v[],int tam){
    buildMaxHeap(v,tam);
    int n = tam;
 
    for (int i = tam - 1; i > 0; i--) {
        trocar(v, i, 0);
        maxHeapify(v, 0, --n);
    }  
}

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	tempos[0] = clock();
	heapSort(vetorentrada, tam); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (HeapSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
