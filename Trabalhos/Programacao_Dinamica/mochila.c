#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

typedef struct $
{
	int valor;   // Representa o valor na matriz 
    int flag;    // Representa se o produto foi usado ou nao
}tMatriz;

void criaVetordeEntrada(char const *argumento[]);
tMatriz max(int i, int j);

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void algoritmoMochila(int produtos[], int custos[],int tamProdutos, int tamMochila){
    
    tMatriz matriz[tamMochila+1][tamProdutos+1];
    		
    int i,
    	j,
    	solucao,
    	aux;

    for (i = 0; i <= tamMochila; i++){
    	matriz[i][tamProdutos].valor = 0;
    	matriz[i][tamProdutos].flag = 0;
    }
    for (i = 0; i < tamProdutos; i++){
    	matriz[0][i].valor = 0;
    	matriz[0][i].flag = 0;
    }

    for (j = tamProdutos - 1; j >= 0; j--){
    	for (i = 1; i <= tamMochila; i++){
    		
    		if (produtos[j] > i){
    			matriz[i][j].valor = matriz[i][j+1].valor;
    			matriz[i][j].flag = 0;	
    		}
    		else{

    			matriz[i][j] = max(matriz[i][j+1].valor,(matriz[i-produtos[j]][j+1].valor + custos[j]));

    		}
       	}
    }

    solucao = matriz[tamMochila][0].valor;

    /*printf("\n");
    for (i = 0; i <= tamMochila; i++){
    	printf("\n");
    	for(j=0; j <= tamProdutos; j++){
	   		printf("%d-%d \t",matriz[i][j].valor,matriz[i][j].flag);
    	}
    }*/

    aux = tamMochila;
    
    printf("\nValor da solucao: %d",solucao);
    printf("\nProdutos escolhidos:");
    for (j = 0; j < tamProdutos; j++){
    	
    	if(matriz[aux][j].flag == 1){
    		printf("|%d|",j+1);
    		aux = aux - produtos[j];
    	}
    	
    }
}

tMatriz max(int esquerda, int direita){
	
	tMatriz resposta;

	resposta.valor = esquerda > direita ? esquerda : direita;
	resposta.flag = esquerda > direita ? 0 : 1;
	
	return resposta;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i,
		j,
	 	aux,
	 	tamMochila,
	 	tamProdutos;
	
	char nomearquivo[] = "instancias/";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tamProdutos);
		fscanf(arquivo,"%d", &tamMochila);
	}

	int produtos[tamProdutos];
	int custos[tamProdutos];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tamProdutos); i++){ 
			fscanf(arquivo,"%d", &aux);
			produtos[i] = aux;
			fscanf(arquivo,"%d", &aux);
			custos[i] = aux;
		}
	}



	tempos[0] = clock();
	algoritmoMochila(produtos,custos,tamProdutos,tamMochila); 
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i].peso);
	}*/

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gasto: %.2lf s", tempo_gasto);

	fclose(arquivo);
	
}