#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

void criaVetordeEntrada(char const *argumento[]);


void countingSort(int v[], int tam){
		
	int maior = v[0];
	

	for (int i = 1; i < tam; i++) {
		    if (v[i] > maior) {
				maior = v[i];
			}
	}

			
// frequencia
	
	int c[maior+1];
	
	for (int i = 0; i < (maior+1); i++) {
			c[i] = 0;
	}


	for (int i = 0; i < tam; i++) {
			c[v[i]] += 1;
	}
		
	//printf("uahuahauh\n");
// cumulativa
	for (int i = 1; i < maior+1; i++) {
			c[i] += c[i-1];
	}
		
	int b[tam];

	printf("\n");
	for (int i = 0; i < maior+1; ++i)
	{
		printf("%d  ",c[i]);
	}
	
	for (int i = tam-1; i >= 0; i--) {
			b[c[v[i]]-1] = v[i];
			printf("\n%d",v[i]);
			c[v[i]]--;
	}
		
	for (int i = 0; i < tam; i++) {
			v[i] = b[i];
	}
}

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	tempos[0] = clock();
	countingSort(vetorentrada, tam); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (CountingSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
