#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>

void criaVetordeEntrada(char const *argumento[]);


void radixsort(int vetor[], int tamanho) {
    int i;
    int *b;
    int maior = vetor[0];
    int exp = 1;

    b = (int *)calloc(tamanho, sizeof(int));

    for (i = 0; i < tamanho; i++) {
        if (vetor[i] > maior)
    	    maior = vetor[i];
    }

    while (maior/exp > 0) {
        int couting[10] = { 0 };
    	for (i = 0; i < tamanho; i++)
    	    couting[(vetor[i] / exp) % 10]++;
    	for (i = 1; i < 10; i++)
    	    couting[i] += couting[i - 1];
    	for (i = tamanho - 1; i >= 0; i--)
    	    b[--couting[(vetor[i] / exp) % 10]] = vetor[i];
    	for (i = 0; i < tamanho; i++)
    	    vetor[i] = b[i];
    	exp *= 10;
    }

    free(b);
}

int main(int argc, char const *argv[]){
	criaVetordeEntrada(argv);
	printf("\nFim\n");
	return 0;
}

void criaVetordeEntrada(char const *argumento[]){
	
	int i, aux, tam;
	char nomearquivo[] = "instancias/";
	char saida[] = "saidas/resposta-";
	clock_t tempos[2];
	
	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
	
	FILE *arquivo = fopen(nomearquivo, "r");
	
	for(i = 0; i < strlen(nomearquivo); i++){nomearquivo[i] = saida[i];}

	strcat(nomearquivo,argumento[1]);
	printf("\n%s",nomearquivo);
		
	FILE *arquivo2 = fopen(nomearquivo, "w");

	if(arquivo != NULL){
		fscanf(arquivo,"%d", &tam);
		printf("\ntamanho: %d", tam);
	}

	int vetorentrada[tam];

	//
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < (tam); i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	tempos[0] = clock();
	radixsort(vetorentrada, tam); //Chamada do algoritmo de ordenacao
	tempos[1] = clock();
	/*for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/

	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}

	double tempo_gasto = ( (double) (tempos[1] - tempos[0]) ) / CLOCKS_PER_SEC;
	printf("\nTempo gastado na ordenacao (RadixSort): %.2lf s", tempo_gasto);

	fclose(arquivo);
	fclose(arquivo2);
}
